info = {
    "name": "Improved Shortcuts",
    "category": "Sequencer",
}

import bpy

is_playing_forward = True
is_playing = False
is_fast = True

class hide_unhide(bpy.types.Operator):
    """improved Hide function"""       # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "object.hide_unhide"   # unique identifier for buttons and menu items to reference.
    bl_label = "Hide / unhide"         # display name in the interface.
    bl_options = {'REGISTER', 'UNDO'}  # enable undo for the operator.

    def execute(self, context):        # execute() is called by blender when running the operator.
        for obj in bpy.context.selected_sequences:
            obj.mute = not obj.mute

        return {'FINISHED'}            # this lets blender know the operator finished successfully.
    
class playback_improved(bpy.types.Operator):
    """Playback Improved"""       # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "sequence.playback_improved"    # unique identifier for buttons and menu items to reference.
    bl_label = "Adding fastforward und backward"  # display name in the interface.

    reverse = bpy.props.BoolProperty(name="reverse", default=False)
    stop = bpy.props.BoolProperty(name="stop", default=False)

    def execute(self, context):
        scn = bpy.context.scene
        global is_fast
        global is_playing
        global is_playing_forward
        
        if (self.stop):
            bpy.ops.screen.animation_cancel(restore_frame=False)
            scn.render.fps = 25
            is_fast = True
            is_playing = False
            is_playing_forward = False
        else:
            if (is_playing == False):
                bpy.ops.screen.animation_play(self.reverse)
                is_playing = True
                is_playing_forward = False
            else:
                if (is_playing_forward != self.reverse):
                    bpy.ops.screen.animation_cancel(restore_frame=False)
                    bpy.ops.screen.animation_play(reverse=self.reverse)
                    is_playing_forward = self.reverse
                else:       
                    if (is_fast):
                        scn.render.fps = scn.render.fps * 2
                        is_fast = False
                    else :
                        scn.render.fps = scn.render.fps / 2
                        is_fast = True       
        
        return {'FINISHED'}

class setup_in_out(bpy.types.Operator):
    """Setting In and Out"""       # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "sequence.setup_in_out"    # unique identifier for buttons and menu items to reference.
    bl_label = "Setting In and Out"  # display name in the interface.
    
    setup_input = bpy.props.BoolProperty(name="setup_input", default=False)
    
    def execute(self, context):
        scn = bpy.context.scene
        if (self.setup_input):
            scn.frame_start = scn.frame_current
        else:
            scn.frame_end = scn.frame_current
        return {'FINISHED'}
    
class goto_in_out(bpy.types.Operator):
    """Go to In and Out"""       # blender will use this as a tooltip for menu items and buttons.
    bl_idname = "sequence.goto_in_out"    # unique identifier for buttons and menu items to reference.
    bl_label = "Go to In and Out"  # display name in the interface.
    
    setup_input = bpy.props.BoolProperty(name="setup_input", default=False)
    
    def execute(self, context):
        scn = bpy.context.scene
        if (self.setup_input):
             scn.frame_current = scn.frame_start
        else:
            scn.frame_current = scn.frame_end
        return {'FINISHED'}

# store keymaps here to access after registration
addon_keymaps = []

def register():    
    bpy.utils.register_class(hide_unhide)
    bpy.utils.register_class(playback_improved)
    bpy.utils.register_class(setup_in_out)
    bpy.utils.register_class(goto_in_out)
    
    # KEYMAP
    wm = bpy.context.window_manager

    # ZOOM In and Out
    km = wm.keyconfigs.addon.keymaps.new('View2D', space_type='EMPTY', region_type='WINDOW', modal=False)
    kmi = km.keymap_items.new('view2d.zoom_in', 'Z', 'PRESS')
    kmi = km.keymap_items.new('view2d.zoom_out', 'Z', 'PRESS', shift=True)
 
    km = wm.keyconfigs.addon.keymaps.new('Sequencer', space_type='SEQUENCE_EDITOR', region_type='WINDOW', modal=False)
 
    # Zoom in and out
    # dot (numpad) to zoom in / all to sequence.
    #kmi = km.keymap_items.new('sequencer.view_selected', 'Z', 'PRESS', alt=True)
    
    ## CURSOR
    # Playback Improved, with Fast and Backforward
    # note: using QSD and not JKL because K is already "cut in place"
    kmi = km.keymap_items.new(playback_improved.bl_idname, 'D', 'PRESS')
    kmi.properties.reverse = False
    kmi.properties.stop = False
    kmi = km.keymap_items.new(playback_improved.bl_idname, 'Q', 'PRESS')
    kmi.properties.reverse = True
    kmi.properties.stop = False
    kmi = km.keymap_items.new(playback_improved.bl_idname, 'S', 'PRESS')
    kmi.properties.reverse = True
    kmi.properties.stop = True
    
    # Setup IN OUT
    kmi = km.keymap_items.new(setup_in_out.bl_idname, 'I', 'PRESS')
    kmi.properties.setup_input = True
    kmi = km.keymap_items.new(setup_in_out.bl_idname, 'O', 'PRESS')
    kmi.properties.setup_input = False
    # Go to IN OUT
    # Note: S-o is already mapped with Context-Toggle (seems unuseful)
    kmi = km.keymap_items.new(goto_in_out.bl_idname, 'I', 'PRESS', shift=True)
    kmi.properties.setup_input = True
     # Note: need to remove the actual shortcuts for S-o :: Context-Toggle 
    kmi = km.keymap_items.new(goto_in_out.bl_idname, 'O', 'PRESS', shift=True)
    kmi.properties.setup_input = False
    
    ## HIDING
    # H to Hide / Unhide
    kmi = km.keymap_items.new(hide_unhide.bl_idname, 'H', 'PRESS')
    
    ## MOVING ##
    
    # Alt + Arrow to move sequence up, down, left and right
    kmi = km.keymap_items.new('transform.seq_slide', 'UP_ARROW', 'PRESS', alt=True)
    kmi.properties.value = (0,1)
    kmi = km.keymap_items.new('transform.seq_slide', 'DOWN_ARROW', 'PRESS', alt=True)
    kmi.properties.value = (0,-1)
    kmi = km.keymap_items.new('transform.seq_slide', 'RIGHT_ARROW', 'PRESS', alt=True)
    kmi.properties.value = (1,0)
    kmi = km.keymap_items.new('transform.seq_slide', 'LEFT_ARROW', 'PRESS', alt=True)
    kmi.properties.value = (-1,0)
    
    # add Shift to move left or right with 10 frames
    kmi = km.keymap_items.new('transform.seq_slide', 'RIGHT_ARROW', 'PRESS', alt=True, shift=True)#
    kmi.properties.value = (10,0)
    kmi = km.keymap_items.new('transform.seq_slide', 'LEFT_ARROW', 'PRESS', alt=True, shift=True)
    kmi.properties.value = (-10,0)

    addon_keymaps.append(km)


def unregister():
    bpy.utils.unregister_class(hide_unhide)
    bpy.utils.unregister_class(playback_improved)
    bpy.utils.unregister_class(setup_in_out)
    bpy.utils.unregister_class(goto_in_out)
    
    # handle the keymap
    wm = bpy.context.window_manager
    for km in addon_keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)
    # clear the list
    addon_keymaps.clear()


# This allows you to run the script directly from blenders text editor
# to test the addon without having to install it.
if __name__ == "__main__":
    register()