# README

Improving shortcuts for blender.

Just download and import vse_improved_shortcuts.py

*Done*
* H : Hide / Unhide function (with the same key)
* Q, S, D : JKL-like function (moving backward (fast backward), stop, forward (fastforward))
* Alt+Arrow to move Clip (up, down, right, left), Alt-Shift + Arrow to move by ten units (only left and right)
* I / O : Setup IN / OUT
* Shift I / O : Goto IN / OUT

*To Do*
* Replace Clip when moving another clip over (for now: the clip is just moving after the other)
* Select Clip at Cursor position
* Change Clip selection